// 移植元
// https://js4.in/repo/gitweb.cgi?p=v6don;a=blob;f=app/javascript/mastodon/features/v6don/highlighter.js;hb=HEAD

const Trie = require('substring-trie');

// ↓の配列に絵文字置換対象の文字列を受け取って置換を施した文字列を返すという
// 関数を追加していく
const trlist: any = { pre: [], rec: [], post: [] };

// ユーティリティ
const hesc = (raw: any) => {
	let ent = false;
	return raw.replace(/./ug, (c: any) => {
		if (ent) {
			if (c === ';') ent = false;
		} else if (c === '&') {
			ent = true;
		} else {
			c = `&#${c.codePointAt(0)};`;
		}
		return c;
	});
};

const unesc = (str: string) => {
	if (str.indexOf('<') !== -1 || str.indexOf('>') !== -1) {
		throw new Error('can\'t unescape string containing tags');
	}
	require('jsdom-global')();
	const elem = document.createElement('div');
	elem.innerHTML = str;
	return elem.textContent;
};

export const ununesc = (str: string) => str.replace(/[&<>]/g, (e: string) => {
	const esc: { [key: string]: string } = { '&': '&amp;', '<': '&lt;', '>': '&gt;' };
	return esc[e];
});

const apply_without_tag = (f: any) => (str: any, rec: any) => {
	let rtn = '';
	const origstr = str;
	let brokentag;
	let depth = 0;
	while (str) {
		let tagbegin = str.indexOf('<');
		const notag = tagbegin === -1;
		if (notag) {
			tagbegin = str.length;
		}
		// < か末尾に到達する前に > に遭遇する場合に備える
		for (let gt = str.indexOf('>'); gt !== -1 && gt < tagbegin; tagbegin -= gt + 1, gt = str.indexOf('>')) {
			rtn += str.slice(0, gt) + '>';
			str = str.slice(gt + 1);
			brokentag = true;
		}
		const pretag = str.slice(0, tagbegin);
		rtn += tagbegin ? ( depth ? pretag : f(pretag, rec) ) : ''; // eslint-disable-line no-nested-ternary
		if (notag) break;

		const tagend = str.indexOf('>', tagbegin + 1) + 1;
		if (!tagend) {
			brokentag = true;
			rtn += str.slice(tagbegin);
			break;
		}
		const tag = str.slice(tagbegin, tagend);
		rtn += tag;
		str = str.slice(tagend);
		if (depth) {
			if (tag[1] === '/') { // closing tag
				depth--;
			} else if (tag[tag.length - 2] !== '/') { // opening tag
				depth++;
			}
		} else if (tag === '<span class="v6don-invisible">') {
			depth = 1;
		}
	}
	if (brokentag) console.warn('highlight()に渡された文字列のタグの対応がおかしい → ', origstr);
	return rtn;
};

const split_each_emoji = (str: any, rec: any) => {
	const list = [];
	str = rec ? rec(str) : str;
	while (str) {
		let ei;
		let type;
		if (str[0] === '&') {
			type = 'char';
			ei = str.indexOf(';') + 1;
		} else if (str[0] === '<') {
			const rr = /^<(svg|object)[\s>]/.exec(str);
			if (/^<img\s/.test(str)) {
				type = 'image';
				ei = str.indexOf('>') + 1;
			} else if (rr) {
				type = 'image';
				const etag = `</${rr[1]}>`;
				ei = str.indexOf(etag) + etag.length;
			} else if (str.length > 1 && str[1] === '/') {
				type = str.length > 1 && str[1] === '/' ? 'tagclose' : 'tagopen';
				ei = str.indexOf('>') + 1;
			}
		} else {
			type = 'char';
			ei = str.codePointAt(0) >= 65536 ? 2 : 1;
		}
		list.push({ type: type, str: str.slice(0, ei) });
		str = str.slice(ei);
	}
	return list;
};

const replace_by_re = (re: RegExp, fmt: any) => (str: any, rec: any) => {
	if (re.global) return str.replace(re, typeof(fmt) === 'string' ? fmt : (...args: any[]) => {
		return fmt(...Array.from(args).slice(0, -2).concat(rec));
	});

	let rtn = '';
	for (let rr = re.exec(str); str && rr; rr = re.exec(str)) {
		const replacement = fmt(...rr.concat(rec));
		if (replacement === null) {
			const idx = rr.index + rr[1].length;
			rtn += str.slice(0, idx);
			str = str.slice(idx);
		} else {
			rtn += str.slice(0, rr.index) + replacement;
			str = str.slice(rr.index + rr[0].length);
		}
	}
	return rtn + str;
};

// ここから関数登録
// ^H^H
trlist.pre.push(apply_without_tag((s: any) => {
	let rtn = '';
	s = unesc(s);
	while (s) {
		const rr = /(\^H)+/i.exec(s);
		if (!rr) break;
		const delend = rr.index;
		if (!delend || /\s/.test(s[delend - 1])) {
			rtn += s.slice(0, delend);
			rtn += rr[0];
			s = s.slice(delend + rr[0].length);
			continue;
		}
		let dellen = rr[0].length / 2;
		let delstart = delend;
		while (delstart > 0 && dellen--) {
			if (/[\udc00-\udfff]/.test(s[--delstart])) delstart--;
		}
		if (delstart < 0) delstart = 0;

		rtn += `${ununesc(s.slice(0, delstart))}<del>${hesc(ununesc(s.slice(delstart, delend)))}</del><span class="v6don-invisible">${rr[0]}</span>`;
		s = s.slice(delend + rr[0].length);
	}
	return rtn + ununesc(s);
}));

// 置換をString.replace()に投げるやつ
const byre = [];

byre.push(...[
	{
		// ✨IPv6✨
		order: 'pre',
		re: /(((?:✨[\ufe0e\ufe0f]?)+)( ?IPv6[^✨]*))((?:✨[\ufe0e\ufe0f]?)+)/u,
		fmt: (all: any, skip: any, kira1: any, ipv6: any, kira2: any, rec: any) => {
			const list = split_each_emoji(ipv6, rec);
			if (list.length > 11) {
				return null;
			}
			let delay = 0;
			let rtn = '';
			list.forEach(e => {
				let c;
				if (/^\s/u.test(e.str)) {
					c = e.str;
				} else {
					switch (e.type) {
						case 'char':
						case 'image':
							c = `<span class="v6don-wave" style="animation-delay: ${delay}ms">${e.str}</span>`;
							delay += 100;
							break;
						case 'tagclose':
						case 'tagopen':
							c = e.str;
							break;
					}
				}
				rtn += c;
			});
			return kira1 + rtn + kira2;
		},
	},
	{
		// ††
		order: 'pre',
		re: /((‡+|†+)([^†‡]{1,30}?))(‡+|†+)/,
		fmt: (m: any, skip: any, d1: any, txt: any, d2: any) => {
			if (d1[0] !== d2[0]) return null;
			return `<span class="v6don-tyu2"><span class="v6don-dagger">${d1}</span>${txt}<span class="v6don-dagger">${d2}</span></span>`;
		},
	},
	{
		// ₍₍🥫⁾⁾
		order: 'pre',
		re: /(₍₍|⁽⁽)(\s*)([^₍₎⁽⁾]+?)(\s*)(₎₎|⁾⁾)/g,
		fmt: (all: any, left: any, lsp: any, biti: any, rsp: any, right: any, rec: any) => {
			const l = left === '⁽⁽' ? 1 : 0;
			const r = right === '⁾⁾' ? 1 : 0;
			if (l === r) return all;
			const list = split_each_emoji(biti, rec);
			if (list.length > 5) return all;
			return `${left}${lsp}<span class="v6don-bitibiti">${biti}</span>${rsp}${right}`;
		},
	},
	{
		order: 'pre',
		re: /([|｜])([^《]{1,20})《([^》]{1,30})》/g,
		fmt: (all: any, begin: any, base: any, ruby: any) => {
			if (/^\s+$/.test(base)) return all;
			return `<span class="v6don-invisible">${begin}</span>`
				+ `<ruby>${base}<span class="v6don-invisible">《${ruby}》</span>`
				+ `<rt><span class="v6don-ruby-rt" data-ruby="${hesc(ruby)}"></span></rt></ruby>`;
		},
	},
	{
		order: 'pre',
		re: /([A-Za-z_.\-\u00a0À-ÖØ-öø-ʯ\u0300-\u036f‐'’々\u4e00-\u9fff\uf900-\ufaff\u{20000}-\u{2ebef}]+)《([^》]{1,30})》/ug,
		fmt: (all: any, base: any, ruby: any) => `<ruby>${base}<span class="v6don-invisible">《${ruby}》</span><rt><span class="v6don-ruby-rt" data-ruby="${hesc(ruby)}"></span></rt></ruby>`,
	},
	{
		tag: true,
		order: 'pre',
		re: /(<a\s[^>]*>)(.*?<\/a>)/mg,
		fmt: (all: any, tag: any, text: any) => tag + text.replace(/:/g, '&#58;'),
	},
	{
		order: 'post',
		tag: true,
		re: /(<(?:p|br\s?\/?)>)((\(?)※.*?(\)?))<\/p>/mg,
		fmt: (all: any, br: any, text: any, po: any, pc: any) =>
			/<br\s?\/?>/.test(text) || (po && !pc || !po && pc) ? all : `${/br/.test(br) ? br : ''}<span class="v6don-kozinkanso">${text}</span></p>`,
	},
	{
		order: 'post',
		re: /([えエ][らラ]いっ+|erait+)[!！]*/ig,
		fmt: (erai: any) => {
			let delay = 0;
			return erai.split('').map((c: any) => {
				c = `<span class="v6don-wave" style="animation-delay: ${delay}ms">${c}</span>`;
				delay += 100;
				return c;
			}).join('');
		},
	},
	{
		order: 'post',
		tag: true,
		re: /説(。*\s*(?:$|\n))/igm,
		fmt: '<span class="v6don-setu">説</span>$1',
	},
]);

byre.forEach(e => {
	trlist[e.order || 'rec'].push(e.tag ? replace_by_re(e.re, e.fmt) : apply_without_tag(replace_by_re(e.re, e.fmt)));
});

// trie
const bytrie: any = { pre: {}, rec: {}, post: {} };
/*
	bytrie.rec['熱盛'] = '<img class="emojione" alt="熱盛" src="/emoji/proprietary/atumori.svg" style="width: 3.06em; height: 2em;"/>';
	[
	{ ptn: '5000兆円', img: require('../../../images/v6don/5000tyoen.svg'), h: 1.8 },
	{ ptn: '5000兆', img: require('../../../images/v6don/5000tyo.svg'), h: 1.8 },
	].forEach(e => {
	bytrie.rec[e.ptn] = `<img alt="${hesc(e.ptn)}" src="${e.img}" style="height: ${e.h}em;"/>`;
	});
*/
[
	{ ptn: '✨', fmt: '<span class="v6don-kira">✨</span>' },
	// { ptn: '🤮', fmt: '<img class="emojione" alt="🤮" title=":puke:" src="/emoji/proprietary/puke.png"/>' },
	{ ptn: 'これすき', fmt: '<span class="v6don-koresuki">これすき</span>' },
	{ ptn: '死にたい', fmt: '<span class="v6don-sinitai"><span class="v6don-sinitai-si">死</span><span class="v6don-sinitai-ni">に</span><span class="v6don-sinitai-ta">た</span><span class="v6don-sinitai-i">い</span></span>' },
].forEach(e => {
	bytrie.post[e.ptn] = e.fmt;
});

Object.keys(bytrie).forEach(o => {
	const k = Object.keys(bytrie[o]);
	if (!k.length) return;
	const t = new Trie(k);
	trlist[o].push(apply_without_tag((str: any) => {
		let rtn = '';
		while (str) {
			const match = t.search(str);
			if (match) {
				rtn += typeof bytrie[o][match] === 'string' ? bytrie[o][match] : bytrie[o][match](match);
				str = str.slice(match.length);
			} else {
				const cl = str.codePointAt(0) < 65536 ? 1 : 2;
				rtn += str.slice(0, cl);
				str = str.slice(cl);
			}
		}
		return rtn;
	}));
});

// まとめ

export const highlight = (text: string) => {
	const reclist = [].concat(trlist.rec);
	const rec = (text: any) => reclist.reduce((t, f) => f(t), text);
	return trlist.post.reduce((t: any, f: any) => f(t), rec(trlist.pre.reduce((t: any, f: any) => f(t, rec), text)));
};
